<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // テーブルを作成する
        // 第一引数：テーブル名、この場合tasks
        // 第二引数：無名関数（引数の$tableが、カラムの属性をいい感じに設定してくれる）。この無名関数の結果、カラムが設定され、create tableが実行される
        Schema::create('tasks', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('assigned_from');
            $table->string('assigned_to');
            $table->date('due_date');
            $table->string('task');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * down() は up()の反対を行うだけ。SQLの実行を取り消したいときなど。
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
