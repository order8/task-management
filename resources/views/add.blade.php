@extends('layout')

@section('content')
<div class="container">
    <h2>タスク追加</h2>
    <form action="/" method="POST">
        @csrf
        <div class="form-row">
            <div class="form-group col-lg-4">
                <input type="text" name="assigned_to" class="form-control" id="" placeholder="担当者">
            </div>
            <div class="form-group col-lg-4">
                <input type="text" name="due_date" class="form-control" placeholder="期限" onfocus="(this.type='date')" onblur="(this.type='text')">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-lg-8">
                <input type="text" name="task" class="form-control" placeholder="タスク内容">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-lg-4">
                <button type="submit" class="btn btn-primary">追加</button>
                <button type="submit" class="btn btn-dark">キャンセル</button>
            </div>
        </div>
    </form>
</div>
@endsection