@extends('layout')

@section('content')
<div class="container">
    <h2>タスク一覧</h2>
    <ul class="responsive-table">
      <li class="table-header">
        <div class="col col-1">Task Id</div>
        <div class="col col-2">Task 名</div>
        <div class="col col-3">期限</div>
        <div class="col col-4">ステータス</div>
        <div class="col col-5">担当者</div>
      </li>
      <li class="table-row">
        <div class="col col-1" data-label="Job Id">42235</div>
        <div class="col col-2" data-label="Customer Name">John Doe</div>
        <div class="col col-3" data-label="Amount">$350</div>
        <div class="col col-4" data-label="Payment Status">Pending</div>
        <div class="col col-5" data-label="My Column">hoge</div>
      </li>
      <li class="table-row">
        <div class="col col-1" data-label="Job Id">42442</div>
        <div class="col col-2" data-label="Customer Name">Jennifer Smith</div>
        <div class="col col-3" data-label="Amount">$220</div>
        <div class="col col-4" data-label="Payment Status">Pending</div>
        <div class="col col-5" data-label="My Column">foo</div>
      </li>
      <li class="table-row">
        <div class="col col-1" data-label="Job Id">42257</div>
        <div class="col col-2" data-label="Customer Name">John Smith</div>
        <div class="col col-3" data-label="Amount">$341</div>
        <div class="col col-4" data-label="Payment Status">Pending</div>
        <div class="col col-5" data-label="My Column">bar</div>
      </li>
      <li class="table-row">
        <div class="col col-1" data-label="Job Id">42311</div>
        <div class="col col-2" data-label="Customer Name">John Carpenter</div>
        <div class="col col-3" data-label="Amount">$115</div>
        <div class="col col-4" data-label="Payment Status">Pending</div>
        <div class="col col-5" data-label="My Column">hage</div>
      </li>
    </ul>
</div>
@endsection