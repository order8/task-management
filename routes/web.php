<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TasksController;

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', 'TasksController@index');
Route::get('/note', function () {
    return view('note');
});
Route::get('/add', 'TasksController@create');
Route::post('/', 'TasksController@store');